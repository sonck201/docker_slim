<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$arrMethod = ['POST', 'PUT', 'PATCH', 'DELETE'];
$app->map($arrMethod, '/[{name}]', function (Request $request, Response $response, array $args) {
    $this->logger->info("Slim-Skeleton '/' route with method::" . $request->getMethod());

    return $response->write(json_encode([
        'method' => $request->getMethod(),
        'arg' => $args,
    ]));
});
